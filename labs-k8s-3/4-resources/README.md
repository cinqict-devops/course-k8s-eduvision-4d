# Lab - Setting resource limits and requests
Deploy a hpa to scale a deployment based on cup utilization 

ToDo
- Deploy `deployment.yaml`
  - Take a look at he the resource requests and limits
- Change the resource cpu limit to `2`
- Change the resource cpu request to `1900m`
- What happens and what can you see in the events?