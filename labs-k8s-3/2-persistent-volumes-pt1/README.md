# Lab - Using persistent volumes

Create a deployment which uses a persistent volume.

ToDo

- Create a persistent volume claim
`kubectl apply -f pvc.yaml`
- Describe the Persistent volume claim
`kubectl describe pvc azure-managed-disk`
  - What is the status and why is it in this state?
- Deploy Nginx
`kubectl apply -f deployment.yaml`
- Describe the PVC again and see what happened
- Create a Port-Forward to the nginx pod
> Tip add `> /dev/null 2>&1 &` after the port-forward command to run the command in the background.
- Access the pod from a browser or curl
- Write a file to the volume bound to the pod 
```
kubectl exec volume-demo-<unique string> -- bash -c 'echo "Hello there!" > /usr/share/nginx/html/index.html'
```
- Access the pod from a browser or curl
- Remove the deployment
`kubectl delete deployment volume-demo`
- Re-deploy nginx
- Port-forward
- Access the pod from a browser or curl
