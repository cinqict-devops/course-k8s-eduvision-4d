# Lab - Using a different storage class and access mode

See why you sometimes need a different storage class 

ToDo
- See the storage classes that are available to you
```
kubectl get storageclass
kubectl get sc default
kubectl get sc azurefile
```
- Create a persistent volume claim which uses the default storgage class
`kubectl apply -f pvc-default.yaml`
- Deploy Nginx
`kubectl apply -f deployment.yaml`
- Scale the deployment to 5
`kubectl scale --replicas=5 deployment/volume-demo`
- Get the pod status
- Why are not all pod get to the running state?
`kubectl describe po volume-demo-<unique string>`
- Delete the deployment and persistent volume claim
- Create a persistent volume claim which uses the azurefile storage class
- Deploy Nginx
- Scale to 5
``

