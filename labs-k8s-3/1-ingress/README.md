# Lab - Deploying an Ingress Controller and access applications using ingresses

Installing a `ingess-nginx` ingress controller with an Azure loadbalancer with public IP

ToDo
- Deploy ingress controller
```
kubectl apply -f ingress-controller-aks.yaml
```
- Verify ingress controller
  - use kubectl to verify if the deployment of the ingress controller was succesfull
    - pods
    - services
    - other resources?
  - use kubectl to get the public IP address of the Load Balancer
  - use curl or your local browser to verify that the ingress controller is responding
  >You can update your hosts file to point the FQDNs 
  >- demo.example.com
  >- helloworld.example.com
  >- helloworld.demo.example.com
  
  >to your EXTERNAL_IP
- Verify ingress
  - Create a new namespace test-ingress
  - Deploy the yaml files in test-ingress to the new namespace
  - Check if the deployment of the applications where successfull
  - Verify if you can access the application via the ingress
  > If you're not able to change your hosts file or don't want to you can test by using curl and add the Host header manually

```shell
curl -H "Host: demo.example.com" http://EXTERNAL_IP
curl -H "Host: helloworld.example.com" http://EXTERNAL_IP
curl -H "Host: helloworld.demo.example.com" http://EXTERNAL_IP
```