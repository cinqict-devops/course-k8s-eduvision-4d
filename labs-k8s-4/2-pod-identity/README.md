# Lab - Using Pod Identity
Using Pod Identity to get the metadata of the pod (VMSS) adn get a file from a blob storage using a token

ToDo
- Take a look at the azureidentities resource in the my-app namespace
- Take a look at the nmi daemonset in the kube-system namespace
- Deploy the demo app in the my-app namespace
- Look at the logging of the mni pods
  - you can look at logging on daemonset level using k9s
- Get an access token from the shell of the ubuntu container in the pod
  - Metadata endpoint for a storage token is: `'http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https%3A%2F%2Fstorage.azure.com%2F'`
  - Location of the README.md file is: `https://podidentitydemo.blob.core.windows.net/podidentity/README.md`

  - Use the header `Metadata:true` for the token retrieval
  - Use the headers `x-ms-version: 2017-11-09` and `Authorization: Bearer <token>` to Download the file
