# Lab - Using Helm
Adding a repository and deploy resources in K8s with a chart

ToDo
- Get a list of repositories currently available 
  ```
  helm repo list
  ```
- Add the bitnami repository
  ```
  helm repo add bitnami https://charts.bitnami.com/bitnami
  ```
- List the charts that are available in the bitnami repo
  ```
  helm search repo bitnami
  ```
- Install a Chart
  ```
  helm install nginx bitnami/nginx
  helm list
  ```
- Update a release with new parameters. Set the replicas to 5
  ```
  helm upgrade nginx bitnami/nginx --set replicaCount=5
  ```
- Change the service port to 8080
  ```
  helm show values bitnami/nginx
  https://artifacthub.io/packages/helm/bitnami/nginx
  ```
- Rollback the last release
  ```
  helm history  
  helm rollback nginx 1
  helm history
  ```
- Update the release with a value file
  - inspect the value file
  ```
  helm upgrade nginx bitnami -f custom-values.yml
  ```
- Update the value file to enable metrics