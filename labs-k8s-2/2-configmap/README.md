# Lab - create a secret

```
echo -n 'admin' > ./username.txt
echo -n '1f2d1e2e67df' > ./password.txt
```
- Create the secret
`kubectl create secret generic credentials --from-file ./username.txt --from-file ./password.txt`
- Read the secret
`kubectl get secret credentials -o yaml`
- Can you decode the secret?
- edit the secret with kubectl
```
kubectl edit secret credentials
```
