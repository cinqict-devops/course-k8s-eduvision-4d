# Lab - Creating local development clusters

ToDo

- Create 2 local clusters using K3D
  `k3d cluster create`
- switch between contexts
  ```
  kubectl config current-context
  kubectl config get-contexts
  kubectl config use-context
  ```
- Add an agent
  `k3d node create`
- What's Docker doing?
- Deploy someting?
  `kubectl get pods -A`
- What do you see more?
- `kubectl cluster-info`

## Learning goals

- Get familiar with
  - K3D
